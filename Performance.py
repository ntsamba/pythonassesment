import time


def get_resource_identifier(name):
    time.sleep(1)  # simulate the delay
    if name is 'foo':
        return 'L9UKvnomjq'
    if name is 'bar':
        return '7U9eyOv7M'
    return 'Not found'


cached_api_calls = {}


def check_response_in_memory(name):
    if name in cached_api_calls:
        print(cached_api_calls[name])
    else:
        resource_name = get_resource_identifier(name)
        cached_api_calls[name] = resource_name
        print(resource_name)


for _ in range(0, 100):
    check_response_in_memory('foo')
    check_response_in_memory('bar')
    check_response_in_memory('foo')
    check_response_in_memory('zoo')
    check_response_in_memory('bar')
