from collections import Counter
# find the elements that exist in the first list but not the second
# and the elements that exist in the second, but not in the first
# put this result in into a single list and sort them in ascending order


first = [2, 2, 5, 6, 7, 2, 1, 8, 9, 9]
second = [2, 1, 5, 6, 66, 7, 77]

# Solution not taking duplicates into account
result = sorted(list(set(first).symmetric_difference(set(second))))
print(result)

# Solution taking duplicates into account
no_duplicate_result = sorted((list((Counter(first) - Counter(second)).elements()) +  # Elements in first only
                           list((Counter(second) - Counter(first)).elements())))  # Elements in s second only
print(no_duplicate_result)