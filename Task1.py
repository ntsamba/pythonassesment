colours = ['blue', 'green', 'yellow', 'black', 'orange']

# Generate the following string from the colours list defined above:
# 'blue --> green --> yellow --> black --> orange'

result = (" --> ".join(colours))
print(result)
