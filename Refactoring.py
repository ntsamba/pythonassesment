# Don't modify this
colours = ['blue', 'green', 'yellow', 'black', 'orange']
fruits = ['berry', 'apple', 'banana', 'currant']
# All of the rest below you may modify
# as you please to achieve the desired output

# 1 - ugly
for i in range(len(colours)-1, -1, -1):
    print(colours[i])

# 1 - refactored
for i in reversed(colours):
    print(i)

# 2 - ugly
for i in range(len(colours)):
    print(i, colours[i])

# 2 - refactored
for i, v in enumerate(colours):
    print(i, v)

# 3 - ugly
min_length = min(len(colours), len(fruits))
for i in range(min_length):
    print(colours[i], fruits[i])

# 3 - refactored
for x, y in zip(colours, fruits):
    print(x, y)

# 4 - ugly
if a <= b and f <= g and c <= d and d <= f and b <=c:
    print('pass')
else:
    print('fail')

# 4 - refactored
if a <= b <= c <= d <= f <= g:
    print('pass')
else:
    print('fail')
